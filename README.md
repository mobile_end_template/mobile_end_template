# 模版文档

> 移动端基础框架设置和使用文档

## 框架设置

``` bash
# 用到的框架和基础组件
主框架：vant
辅框架：vux
网络请求框架：axios
视频组件：vue-video-player
二维码组件：vue-qriously
加密组件：JSEncrypt
裁剪组件：clipper
高级加密组件：
    DES加密-CBC模式
    DES解密-CBC模式
ES6模式组件：
    retrofit-cjs[注解组件]
```
```bash
# 页面结构
配置文件目录：config
静态资源目录：assets
外部JS资源目录：static
Vue页面文件目录：components
Vue自定义组件目录：views
路由组件目录：router
工具类：
    api.js api接口文件（特殊请求除外）
    area.js 城市的选择代码组件
    debounce.js 输入完成的回调函数
    des.js 高级加密组件
    https.js 封装的 axios的组件
App.vue 是页面入口，包含裁剪组件的样式文件
main.js 是基础的一些配置文件
debug.log 使用测试环境时打印的出错信息，此文件可能很大，影响使用时可以删除，如果 npm run dev 启动报错，请检查 debug.log 文件是否存在
```
## 使用文档

``` bash
# 常用命令.bat
常用命令.bat 里面有 6 个常用的选择项，如果没有找到需要的选择项，请用 cmd 命令行手动操作

# 规范/标准
一：函数
    1、首字母小写，第二个单词小写
    2、使用 ES6 的箭头函数代替普通函数
    3、for 循环尽量使用 map 代替
    4、尽量结尾处用 ; 结尾（如果加密混淆，后面没有结尾符，100% 报错 -> 但是可以用）
二：请求
    api.js中的请求
        1、如非必要，不准使用 var
        2、常量使用 const
        3、正常接口定义，以 get 开头
        4、不正常的接口定义，以 as 开头
        5、请不要注入接口更改请求，如果需要使用特殊请求，请参照 axios 标准的文档，改项目保留原生文档的所有使用方案
        6、异步请求中不要夹杂同步请求
        7、同步请求中不要操作 UI
三：其他
    其他要求规范请参照：
        Login.vue
        My.vue
        Register.vue
        NewsDetails.vue
        Invite.vue
    这五个页面，涵盖了整个项目 70% 以上的请求模版
    规范以 ECMAScript 2018 为主，ES6 标准语法为辅
四：Vue页面JS执行顺序(示例如下)
		components: { // 组件},
		props: [ // 组件选项],
		data() { // 数据
			return {

			}
		},
		beforeCreate() { // 创建 vue 实例之前},
		created() { // 创建 vue 实例之后},
		beforeMount() { // 挂载到 dom 之前},
		mounted() { // 挂载到 dom 之后},
		beforeUpdate() { // 数据变化更新之前},
		updated() { // 数据变化更新之后},
		beforeDestroy() { // vue 实例销毁之前},
		destroyed() { // vue 实例销毁之后},
		methods: { // 函数},
		filter: { // 过滤器},
		computed: { // 监听器(计算属性)},
		watch: { // 观察数据变化}


五：有待补充
```
``` bash
# 参考文档
    vant：
        https://youzan.github.io/vant/#/zh-CN/intro

    vux：
        https://doc.vux.li/zh-CN/components/actionsheet.html

    retrofit-cjs：
        https://tencent.信息/archives/69.html

    ES6解构赋值文档：
        https://tencent.信息/archives/67.html

    其他需要的信息可在：
        https://tencent.信息
    进行查找

    CSS文档：
        https://www.jianshu.com/p/8a7ec8b373bd

    JS 外观模式文档：
        https://www.cnblogs.com/linda586586/p/4237093.html

    JS 标准参考文档：
        http://javascript.ruanyifeng.com/#toc3

    全局标准：
        https://cloud.tencent.com/developer/devdocs
    遵循腾讯开发者手册

```

