import { get, post } from './http';

/**
 * @param {Base} [Base Api]
 */
export const base = {
		/**
		 * 帐号登录
		 * @param {POST} Method [请求的类型]
		 */
		getSignUp(res) {
			return post(process.env.API_HOST + 'signUp', res);
		},
		/**
		 * 获取凭证
		 * @param {GET} Method [请求的类型]
		 */
		getCredentials(res) {
			return get(process.env.API_HOST + 'account/credentials', res);
		},
		/**
		 *  帐号注册,忘记密码的找回密码获取短信验证码
		 * @param {POST} Method [请求的类型]
		 */
		getCode(res) {
			return post(process.env.API_HOST + 'validate/code', res);
		},
		/**
		 *  获取注册协议
		 * @param {POST} Method [请求的类型]
		 */
		getRegisterDiscovers(res) {
			return post(process.env.API_HOST + 'admin/operating/discovers/5', res);
		},
		/**
		 *  帐号注册
		 * @param {POST} Method [请求的类型]
		 */
		getRegister(res) {
			return post(process.env.API_HOST + 'account/register', res);
		},
		/**
		 *  忘记密码的找回密码
		 * @param {POST} Method [请求的类型]
		 */
		getForgetPwd(res) {
			return post(process.env.API_HOST + 'account/forget/password', res);
		},
		/**
		 *  获取本地应用资源版本号
		 * @param {GET} Method [请求的类型]
		 */
		getUpdate(res) {
			return get(process.env.API_HOST + 'apps/update.json', res);
		},
		/**
		 *  退出登录
		 * @param {POST} Method [请求的类型]
		 */
		getSignOut(res) {
			return post(process.env.API_HOST + 'account/signOut', res);
		}
}

/**
 * @param {Home} [Home Api]
 */
export const home = {
		/**
		 * 首页轮播图
		 * @param {GET} Method [请求的类型]
		 */
		getBanner(res) {
			return get(process.env.API_HOST + 'admin/operating/banner', res);
		}
}

/**
 * @param {Find} [Find Api]
 */
export const find = {
		/**
		 * 发现页面的内容
		 * @param {GET} Method [请求的类型]
		 */
		getDiscover(res) {
			return post(process.env.API_HOST + 'admin/operating/discover', res);
		}
}

/**
 * @param {NewDetails} [NewDetails Api]
 */
export const newDetails = {
		/**
		 * 发现页面的内容
		 * @param {GET} Method [请求的类型]
		 */
		getDiscover(res) {
			return get(process.env.API_HOST + 'admin/operating/discover', res);
		}
}

/**
 * @param {My} [My Api]
 */
export const mine = {
		/**
		 *  查询个人信息
		 * @param {GET} Method [请求的类型]
		 */
		getPersonal(res) {
			return get(process.env.API_HOST + 'admin/account/personal', res);
		},
		/**
		 *  查询是否有新消息
		 * @param {GET} Method [请求的类型]
		 */
		getIsRead(res) {
			return get(process.env.API_HOST + 'product/isRead', res);
		},
		/**
		 *  查询我的福利
		 * @param {GET} Method [请求的类型]
		 */
		getWelfare(res) {
			return get(process.env.API_HOST + 'account/welfare', res);
		},
		/**
		 *  查询签到状态
		 * @param {GET} Method [请求的类型]
		 */
		getSecond(res) {
			return get(process.env.API_HOST + 'admin/account/second', res);
		},
		/**
		 *  签到
		 * @param {GET} Method [请求的类型]
		 */
		getSign(res) {
			return get(process.env.API_HOST + 'admin/account/sign', res);
		},
		/**
		 *  初始化 websocket 之前需要查询的数据
		 * @param {GET} Method [请求的类型]
		 */
		getUserId(res) {
			return get(process.env.API_HOST + 'admin/message/userId', res);
		}
}

/**
 * @param {Invite} [Invite Api]
 */
export const invite = {
		/**
		 * 获取分享注册页面的二维码信息
		 * @param {GET} Method [请求的类型]
		 */
		getQr(res) {
			return get(process.env.API_HOST + 'admin/account/my/sharing', res);
		},
		/**
		 * 保存二维码 web 端的接口
		 * @param {GET} Method [请求的类型]
		 */
		getDown(res) {
			return get(process.env.API_HOST + 'product/pic', res);
		}
}

/**
 * @param {ModifyPersonal} [ModifyPersonal Api]
 */
export const modifyPersonal = {
		/**
		 * 获取个人信息页面的设置信息
		 * @param {GET} Method [请求的类型]
		 */
		getModifyPersonal(res) {
			return get(process.env.API_HOST + 'admin/account/setting', res);
		},
		/**
		 * 获取完善资料的提示信息
		 * @param {GET} Method [请求的类型]
		 */
		getMaterial(res) {
			return get(process.env.API_HOST + 'admin/account/material', res);
		},
		/**
		 * 更新个人资料信息
		 * @param {POST} Method [请求的类型]
		 */
		getUpdate(res) {
			return post(process.env.API_HOST + 'admin/account/upsetting', res);
		}
}

/**
 * @param {Safety} [Safety Api]
 */
export const safety = {
		/**
		 * 安全设置页面判断支付密码
		 * @param {GET} Method [请求的类型]
		 */
		getTransaction(res) {
			return get(process.env.API_HOST + 'account/transaction', res);
		}
}
