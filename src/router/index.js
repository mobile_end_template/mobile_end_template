import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

export default new Router({
	linkActiveClass: 'on',
	routes: [{
		path: '/',
		component: () =>
			import('@/components/Index'),
		children: [{
			path: '',
			name: 'Home',
			component: () =>
				import('@/components/Home/Home'),
			meta: {
				title: '首页'
			}
		},{
			path: '/Mall',
			name: 'Mall',
			component: () =>
				import('@/components/Mall/Mall'),
			meta: {
				title: '商城'
			}
		},{
			path: '/Find',
			name: 'Find',
			component: () =>
				import('@/components/Find/Find'),
			meta: {
				title: '发现'
			}
		},{
			path: '/My',
			name: 'My',
			component: () =>
				import('@/components/My/My'),
			meta: {
				requiresAuth: true,
				title: '我的'
			}
		}]
	},{
		path: '/Login',
		name: 'Login',
		component: () =>
			import('@/components/Base/Login'),
		meta: {
			title: '登录'
		}
	},{
		path: '/Register',
		name: 'Register',
		component: () =>
			import('@/components/Base/Register'),
		meta: {
			title: '注册'
		}
	},{
		path: '/Qr',
		name: 'Qr',
		component: () =>
			import('@/components/Base/Qr'),
		meta: {
			title: '扫一扫'
		}
	},{
		path: '/FindPwd',
		name: 'FindPwd',
		component: () =>
			import('@/components/Base/FindPwd'),
		meta: {
			title: '忘记密码'
		}
	},{
		path: '/NewsDetails',
		name: 'NewsDetails',
		component: () =>
			import('@/components/Find/NewsDetails'),
		meta: {
			title: '公告详情'
		}
	},{
		path: '/Invite',
		name: 'Invite',
		component: () =>
			import('@/components/My/Invite'),
		meta: {
			requiresAuth: true,
			title: '分享注册'
		}
	},{
		path: '/Setting',
		name: 'Setting',
		component: () =>
			import('@/components/My/Setting'),
		meta: {
			requiresAuth: true,
			title: '设置'
		}
	},{
		path: '/ModifyPersonal',
		name: 'ModifyPersonal',
		component: () =>
			import('@/components/My/ModifyPersonal'),
		meta: {
			requiresAuth: true,
			title: '个人信息'
		}
	},{
		path: '/Safety',
		name: 'Safety',
		component: () =>
			import('@/components/My/Safety'),
		meta: {
			requiresAuth: true,
			title: '安全设置'
		}
	}]
})